const User = require('../model/User')
const userController = {
  userList: [
    {
      id: 1,
      name: 'Bowornchat',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Yongtong',
      gender: 'M'
    }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    User.updateOne({ id: payload.id }, payload)
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    // res.json(userController.getUsers())

    // User.find({}).exec(function (err, users) {
    //   if (err) {
    //     res.status(500).send()
    //   }
    //   res.json(users)
    // })

    // User.find({}).then(function (users) {
    //   res.json(users)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    try {
      const user = await User.find({})
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    // res.json(userController.getUser(id))
    // User.findById(id).then(function (users) {
    //   res.json(users)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
