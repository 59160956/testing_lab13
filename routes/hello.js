var express = require('express')
var router = express.Router()

router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello My Name Is Max', params })
})
module.exports = router
